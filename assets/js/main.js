const calc = document.querySelector('.calculator-body');
const screenEquation = document.querySelector('.calculator-screen__equation');
const screenOutput = document.querySelector('.calculator-screen__output');

function updateScreenEquation(elementValue) {
    if (screenEquation.innerText == 0 && parseFloat(elementValue)) {
        screenEquation.innerText = '';
    }

    if (!parseFloat(elementValue) && elementValue !== 'C' && elementValue != '0' && !parseFloat(screenEquation.innerText[screenEquation.innerText.length - 1]) && screenEquation.innerText[screenEquation.innerText.length - 1] !== 'C' && screenEquation.innerText[screenEquation.innerText.length - 1] != '0' && screenEquation.innerText[screenEquation.innerText.length - 1] != ')') {

        let equation = screenEquation.innerText.split('')
        equation[screenEquation.innerText.length - 1] = elementValue;
        screenEquation.innerText = equation.join('') + ' ';

    } else {
        switch (elementValue) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case ',':
                screenEquation.innerHTML = screenEquation.innerHTML + elementValue;
                break;
            case 'x':
            case '/':
            case '+':
            case '-':
            case '%':
                screenEquation.innerHTML = screenEquation.innerHTML + ' ' + elementValue + ' ';
                break;
            case 'C':
                screenEquation.innerHTML = '0'
                break;
            case '-(':
                screenEquation.innerHTML = '-(' + screenEquation.innerHTML + ') ';
                break;
            case '=':
                screenEquation.innerHTML = screenOutput.innerHTML;
                break;
        }
    }

}

function updateScreenOutput(elementsArray) {
    if (!(elementsArray[elementsArray.length - 1] === 'x' || elementsArray[elementsArray.length - 1] === '/' || elementsArray[elementsArray.length - 1] === '+' || elementsArray[elementsArray.length - 1] === '-' || elementsArray[elementsArray.length - 1] === '%')) {
        let equation = screenEquation.innerText.replaceAll('x', '*').replaceAll('%', '/100*').replaceAll(',', '.');
        equation = eval(equation);

        screenOutput.innerText = equation;
    } else {
        screenOutput.innerText = screenOutput.innerText;
    }
}

calc.addEventListener('click', (e) => {

    updateScreenEquation(e.target.value);
    updateScreenOutput(screenEquation.innerText.split(' '));

})